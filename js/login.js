'use strict';
var bigBtn;
var isLoggedIn = false;

function byId(id) {
	return document.getElementById(id);
}

function byClass(className) {
	return document.getElementsByClassName(className);
}

function showButtons() {
	var btnHldr = byId("main_menu_hldr");
	var bigBtn = byId("big_btn");
	var btns = byClass("choice_btn");

	var origRect = btnHldr.getBoundingClientRect();
	var origRad = origRect.width/2;

	var oldBtnX = parseInt(window.getComputedStyle(btns[0]).getPropertyValue("left"));
	var oldBtnY = parseInt(window.getComputedStyle(btns[0]).getPropertyValue("top"));

	var newBigBtnWidth = origRect.width*66 / origRect.width;
	var transBase = 33;
	var newBtnPos = [];

	var degThingy = 180 / (btns.length + 2);
	var degy;
	for (var i=0; i < btns.length; i++) {
		degy = 180 + (1.5*degThingy + i*degThingy);
		var newTop = 100 * ((oldBtnY + (origRad * Math.sin(degy * (Math.PI/180)))) /origRect.width);
		newTop += transBase/2;
		var newLeft = 100 * ((oldBtnX + (origRad * Math.cos(degy * (Math.PI/180)))) /origRect.width);
		// newLeft += transBase/2;
		newBtnPos[i] = [newLeft + "%", newTop + "%"];
	}

	byId("login_holder").class = "sqr_content hideable";
	byId("big_btn_txt").class = "round_btn_text showable";
	byId("big_btn_txt").innerHTML = "WOOP!";
	byId("login_holder").style.opacity = 0;
	byId("login_holder").style.visibility = "hidden";
	byId("big_btn_txt").style.opacity = 1;
	byId("big_btn_txt").style.visibility = "visible";

	bigBtn.style.width = newBigBtnWidth + "%";
	bigBtn.style.height = newBigBtnWidth + "%";
	bigBtn.style.top = (bigBtn.style.top + transBase) + "%";
	bigBtn.style.left = (bigBtn.style.left + transBase/2) + "%";

	for (var i=0; i< btns.length; i++) {
		btns[i].style.left = newBtnPos[i][0];
		btns[i].style.top = newBtnPos[i][1];
	}
}

function doLogin() {
	var name = byId("user").value;
	var pwd = byId("pwd").value;
	if (name == pwd) {
		sessionStorage.setItem("isLoggedIn", true);
		showButtons();
	}
}

function showLogin(e) {
	bigBtn.removeEventListener("click", showLogin);
	byId("big_btn_txt").style.opacity = "0";
	
	byId('login_holder').style.visibility = "visible"
	byId('login_holder').style.opacity = "1";
	byId('dolog').addEventListener("click", doLogin);
}

function loginInit(e) {
	if (sessionStorage.getItem("isLoggedIn")) {
		isLoggedIn = sessionStorage.getItem("isLoggedIn") ? sessionStorage.getItem("isLoggedIn") : false;
	}
	if (isLoggedIn) {
		showButtons();
	} else {
		bigBtn = document.getElementById('big_btn');
		bigBtn.addEventListener("click", showLogin);
	}
}

window.addEventListener("load", function() {loginInit();});